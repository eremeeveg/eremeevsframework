import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;



public class LoginPass {
  public WebDriver driver;

  public String testUrl = "https://test.a24.biz/";


  @BeforeMethod
  public void testSetup(){

    System.setProperty("webdriver.chrome.driver", "src//main//resources//chromedriveer.exe");

    driver = new ChromeDriver();
    driver.get(testUrl);
  }

  @Test
  public void testRunner() throws InterruptedException {

    WebElement element = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/form/input[2]"));

    element.clear();
    element.sendKeys("123@mail.ru");

    WebElement element1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/form/input[3]"));


    element1.clear();
    element1.sendKeys("12345");

    element.submit();

    Thread.sleep(1000);

    WebElement element3 = driver.findElement(By.xpath("//*[@id=\"root\"]/div[1]/div[2]/div/div/div[1]/div[1]/div/div[1]/div/a/span/span"));

    String nickName = element3.getText();

    System.out.println(nickName);

    Assert.assertEquals(nickName, "Tvinn");

    String currentUrl = driver.getCurrentUrl();

    System.out.println("Страница " + currentUrl);

    Assert.assertEquals(currentUrl, "https://test.a24.biz/home" );



    // home
    // Tvinn
  }

  @AfterMethod
  public void tearDown(){
    driver.quit();
  }

}
