import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class My1stFuckingTest {

    //-----------------------------------Глобальные переменные-----------------------------------
    //Объявляем класс WebDriver
    private WebDriver driver;

    //Объявляем строку с адрессом сайта
    private String testURL = "http://www.google.ru/";

    //-----------------------------------Настройки теста-----------------------------------
    @BeforeMethod
    public void testSetup() {
        //Указываем папку через системный параметр, где находится наш chrome драйвер
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriverr.exe");

        //Создаём новый экземпляр класса ChromeDriver и инициализируем через стандартный конструктор класс WebDriver
        driver = new ChromeDriver();

        //переходим на сайт
        driver.navigate().to(testURL);
    }

    //-----------------------------------Тесты-----------------------------------
   @Test
    public void testRunner() {
        //Получаем заголовок сайта
        String title = driver.getTitle();

        //Выводим заголовок сайта
        System.out.println("Заголовок сайта: " + title);

        //Проверяем (через утвеждение Assert)
        Assert.assertEquals(title, "Google", "Ошибка в названии заголовка!");
    }

    //-----------------------------------После теста(демонтаж)-----------------------------------
    @AfterMethod
    public void tearDown() {
        //Закрываем браузер
        driver.quit();
    }
}
