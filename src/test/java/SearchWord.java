import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.annotations.*;

public class SearchWord {

  public WebDriver driver;

  public String testUrl = "https://www.google.ru/";

  @DataProvider
  public Object [][] moreWords(){
    return new Object[][]{
            {"игра"},
            {"бизнес"},
            {"семья"}
    };
  }

  @BeforeMethod
  public void testSetup () {

    System.setProperty("webdriver.chrome.driver", "src//main//resources//chromedriverr.exe");

    driver = new ChromeDriver();
    driver.get(testUrl);
  }

    @Test (dataProvider = "moreWords")
    public void testRunner(String word){

      WebElement element = driver.findElement(By.cssSelector("#tsf > div:nth-child(2) > div > div.RNNXgb > div > div.a4bIc > input"));
              element.sendKeys(word);
              element.submit();


       WebElement element1 = driver.findElement(By.id("resultStats"));

      System.out.println(element1.getText().replace("Результатов: примерно ", ""));
    }

    @AfterMethod
    public void tearDown(){

      driver.quit();
    }
  }




